(function($) {

    function flag_parent(node) {
        console.log(node);
        if (node.siblings('.live').length == 0) {
            console.log('recursing');
            var parent = node.parent();
            parent.addClass('dead-end');
            flag_parent(parent);
        }
    }

    $(document).ready(function() {
        $('.dead-end').each(function() {
            flag_parent($(this));
        });
    });

})(jQuery);
