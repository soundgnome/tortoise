from django.shortcuts import get_object_or_404, redirect, render
from .models import Node, Scenario


def node_toggle(request, pk):
    node = get_object_or_404(Node, pk=pk)
    node.dead_end = not node.dead_end
    node.save()
    return redirect('tree_scenario', node.scenario.pk)


def scenario(request, pk):
    scenario = get_object_or_404(Scenario, pk=pk)
    return render(request, 'tree/scenario.html', locals())


def scenarios(request):
    scenarios = Scenario.objects.all()
    return render(request, 'tree/scenarios.html', locals())
