from adminsortable.admin import NonSortableParentAdmin, SortableTabularInline
from django.contrib import admin
from .models import Node, Scenario


class NodeInline(SortableTabularInline):
    model = Node
    fields = ('name', 'dead_end')
    extra = 4


class NodeAdmin(NonSortableParentAdmin):
    inlines = (NodeInline,)


class ScenarioAdmin(NonSortableParentAdmin):
    inlines = (NodeInline,)


admin.site.register(Node, NodeAdmin)
admin.site.register(Scenario, ScenarioAdmin)
