from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^node/toggle/([0-9]+)$', views.node_toggle, name='tree_node_toggle'),
    url(r'^scenario/([0-9]+)$', views.scenario, name='tree_scenario'),
    url(r'^scenarios$', views.scenarios, name='tree_scenarios'),
]
