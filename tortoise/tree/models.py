from adminsortable.models import SortableMixin
from django.db import models
from django.urls import reverse


class Scenario(models.Model):
    name = models.CharField(max_length=255)

    def children(self):
        return Node.objects.filter(scenario=self).filter(parent=None)

    def get_absolute_url(self):
        return reverse('tree_scenario', args=[self.pk])

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Node(SortableMixin):
    scenario = models.ForeignKey('Scenario')
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('Node', blank=True, null=True)
    dead_end = models.BooleanField(default=False)
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    def children(self):
        return self.__class__.objects.filter(parent=self)

    def get_absolute_url(self):
        return self.scenario.get_absolute_url()

    def save(self, *args, **kwargs):
        if self.scenario_id is None:
            self.scenario_id = self.parent.scenario_id
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('order',)
